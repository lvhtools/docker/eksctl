FROM python:3-alpine

RUN pip install -U awscli

RUN apk add curl npm --no-cache
RUN curl --silent --location "https://github.com/weaveworks/eksctl/releases/download/latest_release/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp \
    && mv /tmp/eksctl /usr/local/bin

RUN eksctl version
